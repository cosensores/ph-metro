# pHímetro

Herramienta libre para la determinación de pH de soluciones acuosas.

Adaptación del codigo de DFRobot Gravity: Analog pH Sensor / Meter Kit V2, SKU:SEN0161-V2

﻿https://wiki.dfrobot.com/Gravity__Analog_pH_Sensor_Meter_Kit_V2_SKU_SEN0161-V2
﻿https://github.com/DFRobot/DFRobot_PH

Librerias control sensor temperatura: ds18b20

﻿https://github.com/PaulStoffregen/OneWire
﻿https://github.com/milesburton/Arduino-Temperature-Control-Library

![foto](/Imagenes/pHimetro2.jpg)

![foto](/Imagenes/pHimetro1.jpg)

﻿Aplicación: /Aplicación android/pHimetro.apk  
﻿Aplicación editable[(kodular)](https://www.kodular.io/):/Aplicación android/pHimetro.aia
